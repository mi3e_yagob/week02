﻿using UnityEngine;
using System.Collections;

public class RobotController : MonoBehaviour {

    public Animator Mybot;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.W))
        {
            Mybot.SetFloat("Speed", 1);
        }
        else {
            Mybot.SetFloat("Speed", 0);
        }

        if (Input.GetKey(KeyCode.LeftShift))
        {
            Mybot.SetBool("IsSprint", true);
        }
        else {
            Mybot.SetBool("IsSprint", false);
        }

        //if (Input.GetKey(KeyCode.Space))
        //{
        //    Mybot.SetBool("Jump", true);
        //}
        //else {
        //    Mybot.SetBool("Jump", false);
        //}

        if (Input.GetKeyDown(KeyCode.D))
        {
            Mybot.SetTrigger("Attack1");
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            Mybot.SetTrigger("Jump1");
        }
    }
}
