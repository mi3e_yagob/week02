﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

	public float speed;
	public float lifeTime;
	private float timer =0f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		this.gameObject.transform.Translate(Vector3.up*speed*Time.deltaTime);
		timer += Time.deltaTime;
		if(timer >= lifeTime)
		{
			Debug.Log("End life!");
			Destroy(this.gameObject);
		}
	}

	void OnCollisionEnter2D(Collision2D coll) {
		Destroy(this.gameObject);
	}
}
