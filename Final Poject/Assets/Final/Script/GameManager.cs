﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

	public GameObject level01;
	public GameObject level02;

	public bool isEndGame;

	private GameObject player;
	// Use this for initialization
	void Start () {
		player = GameObject.FindObjectOfType<BasePlayer>().gameObject;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void EndLevel(int i)
	{
		if (i == 1)
		{
			level02.SetActive(true);
			level01.SetActive(false);
		}
		if (i == 2)
		{
			level01.SetActive(false);
			level02.SetActive(false);
			isEndGame = true;
		}
		if(i == 3)
		{
			Destroy(player);
			isEndGame = true;
		}
	}
}
