﻿using UnityEngine;
using System.Collections;

public class BaseLevel : MonoBehaviour {

	public int level;

	public float moveTime;
	public float moveTimer;

	private float side = 1;
	public float currentPos = 0;
	public float currentY = 0;
	private int moveCount = 0;

	public int enemyCount;

	public GameManager getGameManager;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(!getGameManager.isEndGame)
		{
			moveTimer += Time.deltaTime;
			if(moveTimer >= moveTime)
			{
				if(moveCount < 5)
				{
					currentPos++;
				}
				else if (moveCount > 5 && moveCount <11)
				{
					currentPos--;
				}
				if(moveCount == 5)
				{
					currentY-=2;
				}
				if(moveCount == 11)
				{
					moveCount = 0;
					currentY-=2;
				}
				CheckLevel();
				moveTimer = 0f;
				this.transform.position = new Vector3(currentPos,currentY,0);
				moveCount++;
			}
		}
	}
	void CheckLevel()
	{
		enemyCount = gameObject.transform.childCount;
		if(enemyCount <= 0)
		{
			getGameManager.EndLevel(level);
		}
		if(currentY <= -6)
		{
			getGameManager.EndLevel(3);
		}
	}
}
