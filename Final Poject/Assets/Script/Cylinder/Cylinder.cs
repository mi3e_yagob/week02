﻿using UnityEngine;
using System.Collections;

public class Cylinder : MonoBehaviour {
	
	public KeyCode leftKey;
	public KeyCode rightKey;
	public KeyCode fireKey;
	
	public GameObject prefabBullet;
	
	public float coolDown;
	private float coolDownTimmer;
	private bool isReadyFire = false;

	public Manager getManager;
	
	void Update () 
	{
		if(!isReadyFire)
		{
			coolDownTimmer += Time.deltaTime;
			if(coolDownTimmer >= coolDown)
			{
				isReadyFire = true;
				coolDownTimmer = 0f;
			}
		}
		if(Input.GetKey(leftKey))
		{
			this.gameObject.transform.Translate(Vector3.left*5*Time.deltaTime);
		}
		if(Input.GetKey(rightKey))
		{
			this.gameObject.transform.Translate(Vector3.right*5*Time.deltaTime);
		}
		if(Input.GetKeyDown(fireKey))
		{
			if(isReadyFire)
			{
				Debug.Log ("Fire!!!");
				Instantiate(prefabBullet,transform.position,Quaternion.identity);
				isReadyFire = false;
			}
		}
	}
	void OnCollisionEnter(Collision Coll)
	{
		Debug.Log ("asdf");
		if (Coll.gameObject.tag == "GoodItem") {
			getManager.UpdateScore(1);
			Debug.Log ("Good");
		}
		else if (Coll.gameObject.tag == "BadItem") {
			getManager.UpdateScore(-1);
			Debug.Log ("Bad");
		}
		Destroy (Coll.gameObject);
	}
}


